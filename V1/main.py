import time
import math
import board
from digitalio import DigitalInOut, Pull, Direction
import pulseio

print("Imports done")

WORK_DURATION = 26 * 60
WORK_NOTIFY = 5 * 60

BREAK_DURATION = 6 * 60
BREAK_NOTIFY = 2 * 60

# No PWM on A/D0, A/D6 !

led = DigitalInOut(board.LED)
led.direction = Direction.OUTPUT

s_green = DigitalInOut(board.D4)
s_green.switch_to_input(pull=Pull.DOWN)

s_orange = DigitalInOut(board.D5)
s_orange.switch_to_input(pull=Pull.DOWN)

l_green = pulseio.PWMOut(board.D7, duty_cycle=0, frequency=256, variable_frequency=False)
l_orange = pulseio.PWMOut(board.D8, duty_cycle=0, frequency=256, variable_frequency=False)


print("GPIO setup done")


def green():
    return s_green.value

def orange():
    return s_orange.value

def clear():
    l_green.duty_cycle = 65535
    l_orange.duty_cycle = 65535


def wait(color, duration, notify, extend=0):
    step = 1

    if color == 'green':
        l_green.duty_cycle = 30000
        l_orange.duty_cycle = 65535
        target = l_green
    else:
        l_green.duty_cycle = 65535
        l_orange.duty_cycle = 30000
        target = l_orange

    start = time.monotonic()
    while True:
        time.sleep(step)
        if orange() and green():
            return False
        time_left = duration - (time.monotonic() - start)
        if time_left <= 0:
            return True
        elif time_left < notify:
            if orange() and extend:
                duration += extend
                target.duty_cycle = 30000
            else:
                target.duty_cycle = 30000 - target.duty_cycle
            


def wave_once(breakable=False):
    for i in range(80):
        l_green.duty_cycle = int(32767 * (1.0 + math.sin(i * math.pi / 40.0)))
        l_orange.duty_cycle = int(32767 * (1.0 + math.sin((40+i) * math.pi / 40.0)))
        time.sleep(0.05)

        if breakable and (green() or orange()):
            return False

    return True


while True:
    clear()

    wave_once()
    while wave_once(True):
        pass

    clear()
    # wait out the button press
    while orange() or green():
        pass

    while True:
        time.sleep(0.1)
        if orange():
            wait('orange', WORK_DURATION, WORK_NOTIFY, 2*WORK_NOTIFY)
            break
        elif green():
            wait('green', BREAK_DURATION, BREAK_NOTIFY)
            break
