import pwmio
import board
from digitalio import DigitalInOut, Pull, Direction
import adafruit_rgbled
import neopixel_write
from micropython import const

STRIP_PIN = board.D6
STRIP_LEN = 5
STRIP_OFF_BYTES = bytes(3 * STRIP_LEN) # all zeros

BUTTON_PIN = board.D4
BUTTON_RGB_PINS = (board.D2, board.D5, board.D1)

TICK_RATE = const(32)
TICK_DURATION = 1.0 / TICK_RATE


def scale_tuple(f: float, color: tuple):
    f = min(1, max(0, f))
    return tuple(int(f * c) for c in color)


def strip_steps_rgb(f: float, color: tuple, scale:float = 1):
    r, g, b = color
    color = (g, r, b)

    buf = bytearray(3 * STRIP_LEN)
    step = 0
    while step < STRIP_LEN and f > 0:
        s = 1 if f > 1 else f
        step_color = bytes(scale_tuple(s * scale, color))
        buf[3 * step: 3 * step + 3] = step_color
        step += 1
        f -= 1.0
    return buf


class Button:
    LONG = TICK_RATE // 2
    MAX_HOLD = 100 * LONG

    def __init__(self, pin):
        self._tick_held = 0
        self._is_held = False
        self._pin = pin

    def update(self):
        if self._pin.value:
            if self._is_held:
                self._tick_held = min(self._tick_held + 1, self.MAX_HOLD)
            else:
                self._tick_held = 1
                self._is_held = True
        else:
            if not self._is_held:
                # second tick after release
                self._tick_held = 0
            else:
                self._is_held = False

    def held(self):
        return self._is_held

    def held_long(self):
        return self._is_held and self._tick_held >= self.LONG

    def short_push(self):
        return not self._is_held and \
                self._tick_held and \
                self._tick_held < self.LONG

class Sound:
    def __init__(self, pin):
        self._pin = pin
        self._pwm = None
        self._sequence = None
        self._wobble = False

    def stop(self):
        if self._pwm is not None:
            self._pwm.deinit()
            self._pwm = None
        self._sequence = None

    def start_tone(self, f):
        if self._pwm is None:
            self._pwm = pwmio.PWMOut(self._pin, variable_frequency=True)

        if f is None or f == 0:
            self._pwm.duty_cycle = 0xffff
            self._pwm.frequency = 100
        else:
            self._pwm.duty_cycle = 0x7fff
            self._pwm.frequency = f

    def update(self):
        if self._sequence is None:
            return

        if self._progress > 0:
            self._progress -= 1
            if self._wobble:
                self._pwm.frequency = self._tone + 2 * (self._progress if self._progress % 2 else -self._progress)
        elif not self._sequence:
            print('Playing finished.')
            self.stop()
            self._sequence = None
        else:
            while self._sequence and self._progress <= 0:
                tone, self._progress = self._sequence[0]
                self._sequence = self._sequence[1:]
            self._wobble = tone < 0
            self._tone = abs(tone)
            self.start_tone(self._tone)

    # sequence is a list of (freq, ticks)
    def play(self, sequence: List):
        print('Playing', sequence)
        self._wobble = False
        self._progress = 0
        self._sequence = sequence


class Board:
    def __init__(self):
        button_pin = DigitalInOut(board.D4)
        button_pin.switch_to_input(pull=Pull.DOWN)
        self.button = Button(button_pin)
        self.button_rgb = adafruit_rgbled.RGBLED(*BUTTON_RGB_PINS, invert_pwm=True)
        self.sound = Sound(board.D3)

        self._strip_pin = DigitalInOut(board.D6)
        self._strip_pin.direction = Direction.OUTPUT

    def update(self):
        self.button.update()
        self.sound.update()

    def set_strip_mono_rgb(self, color: tuple):
        if color == None:
            self.set_strip(STRIP_OFF_BYTES)
        else:
            r, g, b = color
            color = (g, r, b)
            self.set_strip(bytearray(color * STRIP_LEN))

    def set_strip(self, buf: bytearray):
        # TODO: remember last state, don't re-send to save battery and time
        neopixel_write.neopixel_write(self._strip_pin, buf)

    def test_loop(self, t: int):
        import time

        done = t + time.monotonic()
        tick_duration = 1.0 / TICK_RATE

        while True:
            start = time.monotonic()
            if start >= done:
                return
            self.update()
            sleep_time = tick_duration - (time.monotonic() - start)
            if sleep_time > 0:
                time.sleep(sleep_time)
