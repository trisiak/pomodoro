import gc
import time
import math
from rainbowio import colorwheel

from core import Board, Button, Sound, TICK_RATE, STRIP_LEN, strip_steps_rgb, scale_tuple

gc.collect()
print("Imports done. Free:", gc.mem_free())

TWITCH_PURPLE = (60, 24, 180)

# (seconds per step, duration, color)
TIMERS = [
        #(3, 10, TWITCH_PURPLE), # XXX: testing

        (5 * 60, 25 * 60, TWITCH_PURPLE), # work; ~Twitch Purple
        (1 * 60, 5 * 60, (0, 128, 0)),    # short break
        (1 * 60, 10 * 60, (60, 128, 0)),  # long break
]
DEFAULT_NEXT_TIMER = (1, 0, 0)

# Warning: no PWM on A/D0 or A/D6!
"""
BOARD SETUP

D6         -- 5-long strip of neopixels
D4         -- momentary switch, normally LOW
D2, D5, D1 -- RGB LEDs on the switch, common cathode

D3         -- buzzer, anode
A0         -- reserved for the speaker, unused
"""
board = Board() # XXX: don't shadow the board module
print("Board setup done. Free:", gc.mem_free())


class TimerSelect:
    BREATH_TICKS = TICK_RATE * 5
    CONFIRM_TICKS = TICK_RATE * 1.5

    def __init__(self, timer_idx=0):
        self._timer_idx = timer_idx

    def enter(self):
        self._breath = 0 # max brightness
        self._confirm = 0

        board.set_strip_mono_rgb(None)
        board.button_rgb.color = (0, 0, 0)

        self.next_timer(offset=0)

    def next_timer(self, offset=1):
        self._timer_idx += offset
        if self._timer_idx >= len(TIMERS):
            self._timer_idx = 0
        self._timer_color = TIMERS[self._timer_idx][2]

    def update(self):
        if board.button.held_long():
            self._confirm += 1
            if self._confirm >= self.CONFIRM_TICKS:
                board.set_strip_mono_rgb(None)
                board.sound.play([(568, TICK_RATE // 8), (600, TICK_RATE // 8), (-673, TICK_RATE // 6)])
                return InCountdown(self._timer_idx)

            progress = self._confirm / (self.CONFIRM_TICKS - 1)
            board.set_strip(strip_steps_rgb(progress * STRIP_LEN, self._timer_color, scale=0.25))
            board.button_rgb.color = scale_tuple(progress*progress, self._timer_color)
        else:
            self._confirm = 0
            if board.button.short_push():
                self.next_timer()
                self._breath = 0 # set to max brightness
            else:
                self._breath += 1
                if self._breath >= self.BREATH_TICKS:
                    self._breath = 0
        
            br = .5 - self._breath / (self.BREATH_TICKS - 1)
            br = 4 * br * br

            board.button_rgb.color = scale_tuple(br, self._timer_color)
            board.set_strip_mono_rgb(scale_tuple(br / 4, self._timer_color))


class InCountdown:
    ACTIVE_DURATION = 10
    BREATH_TICKS = 2 * TICK_RATE

    def __init__(self, timer_idx: int):
        self._timer_idx = timer_idx
        self._step_duration, self._total_duration, self._color = TIMERS[timer_idx]

    def enter(self):
        self._start = time.monotonic()
        self._done_time = self._start + self._total_duration
        self._activated_till = self._start + self.ACTIVE_DURATION
        self._breath = 0
        print("Starting countdown. Total:", self._total_duration, "step:", self._step_duration)

    def update(self):
        brightness = 1.0 
        now = time.monotonic()
        time_left = self._done_time - now

        if time_left <= 0:
            board.button_rgb.color = 0
            board.set_strip_mono_rgb(None)
            return Finished(self._timer_idx)

        if board.button.held():
            self._activated_till = now + self.ACTIVE_DURATION

        if time_left <= 2 * self._step_duration:
            if board.button.held_long():
                # B, E
                board.sound.play([(494, TICK_RATE // 16), (659, TICK_RATE // 16)])
                self._activated_till = now + self.ACTIVE_DURATION
                self._done_time += 2 * self._step_duration
                self._breath = 0
            else:
                self._breath += 1
                if self._breath >= self.BREATH_TICKS:
                    self._breath = 0
                brightness = 0.5 + abs(self._breath / self.BREATH_TICKS - 0.5)

        if now <= self._activated_till:
            # active
            board.button_rgb.color = scale_tuple(brightness, self._color)
            board.set_strip(strip_steps_rgb(time_left / self._step_duration, self._color, scale=0.25))
        else:
            # inactive
            board.button_rgb.color = scale_tuple(0.5 * brightness, self._color)
            board.set_strip_mono_rgb(None)


class Finished:
    WAIT_TICKS = 5 * TICK_RATE

    def __init__(self, timer_idx: int):
        self._color = TIMERS[timer_idx][2]
        self._next = DEFAULT_NEXT_TIMER[timer_idx]
        self._buffer = bytearray(STRIP_LEN * 3)

    def enter(self):
        self._ticks = 0
        board.set_strip_mono_rgb(self._color)
        board.sound.play([
            (-317, 8),(-460, 2),(580, 2),(-631, 4),
        ])

    def update(self):
        if board.button.short_push():
            board.button_rgb.color = 0
            board.set_strip_mono_rgb(None)
            return TimerSelect(self._next)

        self._ticks += 1
        if self._ticks >= self.WAIT_TICKS:
            board.button_rgb.color = colorwheel(self._ticks % 256)
            board.set_strip_mono_rgb(scale_tuple(0.1, self._color))
            return

        offset = self._ticks * 4 % 256
        board.button_rgb.color = colorwheel(offset)
        offset += 10
        for i in range(len(self._buffer) / 3):
            c = colorwheel(offset + 10 * i).to_bytes(3, 'big')
            # RGB -> GRB
            self._buffer[3*i] = c[1]
            self._buffer[3*i+1] = c[0]
            self._buffer[3*i+2] = c[2]
        board.set_strip(self._buffer)


state = TimerSelect()
state.enter()
tick_duration = 1.0 / TICK_RATE

gc.collect()
while True:
    start = time.monotonic()
    board.update()
    next_state = state.update()
    if next_state is not None:
        print("State transition:", state.__qualname__, "->", next_state.__qualname__)
        next_state.enter()
        state = next_state
        gc.collect()
        print("Free memory (after collection):", gc.mem_free())

    sleep_time = tick_duration - (time.monotonic() - start)
    if sleep_time > 0:
        time.sleep(sleep_time)
